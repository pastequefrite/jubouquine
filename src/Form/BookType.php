<?php

declare(strict_types = 1);

namespace App\Form;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\BookCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'attr' => [
                    'placeholder' => 'Titre du livre',
                ],
            ])
            ->add('author', EntityType::class, [
                'class' => Author::class,
                'label' => 'Auteur',
                'choice_label' => 'name',
            ])
            ->add('bookCategory', EntityType::class, [
                'class' => BookCategory::class,
                'label' => 'Catégorie',
                'choice_label' => 'libelle',
            ])
            ->add('image', FileType::class, [
                'data_class' => null,
                'label' => 'Image',
                'required' => false,
            ])
            ->add('abstract', TextareaType::class, [
                'label' => 'Synopsis',
                'attr' => [
                    'placeholder' => 'Synopsis du livre',
                    'rows' => 3,
                ],
            ])
            ->add('page', IntegerType::class, [
                'label' => 'Pages',
                'attr' => [
                    'placeholder' => 'Nombre de pages du livre',
                ],
            ])
            ->add('rate', IntegerType::class, [
                'label' => 'Note du livre',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Note du livre',
                    'min' => 0,
                    'max' => 5,
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Enregistrer',
                'attr' => [
                    'class' => 'btn-outline-success pull-right',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'data_class' => Book::class,
            ])
        ;
    }
}