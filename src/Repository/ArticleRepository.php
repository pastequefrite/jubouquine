<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    // /**
    //  * @return Article[] Returns an array of Article objects ordered by id DESC
    //  */

    public function findLastArticles($results)
    {
        return $this
            ->createQueryBuilder('article')
            ->orderBy('article.id', 'DESC')
            ->setMaxResults($results)
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return Article[] Returns an array of Article objects ordered by id DESC
    //  */

    public function findLastArticlesFirstMax($firstres, $maxres)
    {
        return $this
            ->createQueryBuilder('article')
            ->orderBy('article.id', 'DESC')
            ->setFirstResult($firstres)
            ->setMaxResults($maxres)
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return Article[] Returns an array of Article objects ordered by id DESC
    //  */

    public function findLastArticlesFirstMaxBefore($firstres, $maxres, $idres)
    {
        return $this
            ->createQueryBuilder('article')
            ->where('article.id < :idres')
            ->setParameter('idres', $idres)
            ->orderBy('article.id', 'DESC')
            ->setFirstResult($firstres)
            ->setMaxResults($maxres)
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
