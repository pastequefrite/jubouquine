<?php

declare(strict_types = 1);

namespace App\EventListener;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\File\File;

class EditUser implements EventSubscriberInterface {

    protected $parameter;
    protected $img;

    public function __construct()
    {
        $this->parameter = 'assets/images/users/';
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::PROFILE_EDIT_INITIALIZE => 'onEditInitialize',
            FOSUserEvents::PROFILE_EDIT_SUCCESS => 'onEditSuccess'
        ];
    }

    public function onEditInitialize(GetResponseUserEvent $event) {
        $this->img = $event->getUser()->getImage();
    }

    public function onEditSuccess(FormEvent  $event) {
        //$user = $event->getUser();
        $user = $event->getForm()->getData();

        //si image post
        if (!is_string($user->getImage()) && $user->getImage() != null) {
            $file = $user->getImage();
            $filename = 'user_' . uniqid('', false) . '.' . $file->guessExtension();
            $file->move($this->parameter, $filename);
            $user->setImage($this->parameter . $filename);
            //dump($this->img);die;
            if ($this->img !== null && !strpos($this->img, 'default.jpeg')) {
                if (file_exists($this->img)) {
                    unlink($this->img);
                }
            }
        }

        if($user->getImage() === null) {
            $user->setImage($this->img);
        }
    }
}