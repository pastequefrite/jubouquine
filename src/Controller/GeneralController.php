<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GeneralController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="app_home")
     */
    public function home(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository(Article::class)->findLastArticles(2);
        return $this->render('front/home.html.twig', [
            'articles' => $articles,
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/mentions-legales", methods={"GET"}, name="app_mentions_legales")
     */
    public function mentionsLegales(): Response
    {
        return $this->render('front/mentions-legales.html.twig');
    }
}