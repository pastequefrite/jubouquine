<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\DeleteType;
use App\Form\RegistrationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/{id<\d+>}/edit", methods={"GET", "POST"}, name="app_user_edit", condition="request.isXmlHttpRequest()")
     */
    public function editUser(Request $request, User $user): Response
    {
        $form = $this->createForm(RegistrationType::class, $user,
            ['action' => $this->generateUrl($request->get('_route'),['id' => $user->getId()])]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'L\'utilisateur a bien été modifié');
            return new Response('success');
        }

        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}/delete", methods={"GET","POST"}, name="app_user_delete", condition="request.isXmlHttpRequest()")
     */
    public function deleteUser(Request $request, User $user): Response
    {
        $form = $this->createForm(DeleteType::class, $user,
            ['action' => $this->generateUrl($request->get('_route'),['id' => $user->getId()])]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            $this->addFlash('success', 'L\'utilisateur a bien été supprimé');
            return new Response('success');
        }
        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}