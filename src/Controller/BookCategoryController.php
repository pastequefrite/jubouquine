<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\BookCategory;
use App\Form\BookCategoryType;
use App\Form\DeleteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bookcategory")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class BookCategoryController extends AbstractController
{
    /**
     * @Route("/new", methods={"GET", "POST"}, name="app_bookcategory_new", condition="request.isXmlHttpRequest()")
     */
    public function newBookCategory(Request $request): Response
    {
        $bookCategory = new BookCategory();

        $form = $this->createForm(BookCategoryType::class, $bookCategory,
            ['action' => $this->generateUrl($request->get('_route'))]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $em = $this->getDoctrine()->getManager();
            $bookCategoryBDD = $em->getRepository(BookCategory::class)->findOneBy(['libelle' => $bookCategory->getLibelle()]);

            if (!$bookCategoryBDD) {
                $this->getDoctrine()->getManager()->persist($bookCategory);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'La catégorie a bien été ajoutée');
                return new Response('success');
            } else {
                $this->addFlash('danger', 'Impossible d\'ajouter la catégorie car elle existe déjà');
                return new Response('danger');
            }
        }

        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}/edit", methods={"GET", "POST"}, name="app_bookcategory_edit", condition="request.isXmlHttpRequest()")
     */
    public function editBookCategory(Request $request, BookCategory $bookCategory): Response
    {
        $form = $this->createForm(BookCategoryType::class, $bookCategory,
            ['action' => $this->generateUrl($request->get('_route'),['id' => $bookCategory->getId()])]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'La catégorie a bien été modifiée');
            return new Response('success');
        }

        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}/delete", methods={"GET","POST"}, name="app_bookcategory_delete", condition="request.isXmlHttpRequest()")
     */
    public function deleteBookCategory(Request $request, BookCategory $bookCategory): Response
    {
        $form = $this->createForm(DeleteType::class, $bookCategory,
            ['action' => $this->generateUrl($request->get('_route'),['id' => $bookCategory->getId()])]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bookCategory);
            $em->flush();
            $this->addFlash('success', 'La catégorie a bien été supprimée');
            return new Response('success');
        }
        return $this->render('front/popup_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}