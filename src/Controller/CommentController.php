<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comment")
 */
class CommentController extends AbstractController
{
    /**
     * @Route("/", methods={"GET, POST"}, name="app_comment")
     */
    /*public function newAction()
    {
        $comment = new Comment();
        $form   = $this->createForm(CommentType::class, $comment);

        dump($form);die;
        return $this->render('front/comment_form.html.twig', [
            'form'   => $form->createView()
        ]);
    }*/


    /**
     * @Route("/new", methods={"GET, POST"}, name="app_comment_new")
     */
    public function newComment(Article $article, Request $request, Comment $comment): Response
    {
        $form   = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        $comment
            ->setUser($this->getUser())
            ->setDate(new \DateTime('now',timezone_open('Europe/Paris')))
            ->setUser($this->getUser())
            ->setArticle($article);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            $this->addFlash('success', 'Le commentaire a bien été ajouté');
            return $this->redirectToRoute('app_article_show', [
                'id' => $article->getId(),
            ]);
        }

        return $this->render('front/article_template.html.twig', [
            'form' => $form->createView(),
            'article' => $article,
        ]);
    }

}